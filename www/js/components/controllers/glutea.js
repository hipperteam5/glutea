(function() {

	var gluteaControllers = angular.module('gluteaControllers', []);

	gluteaControllers.controller('homeController', ['$scope', '$location', 'auth', homeController]);
	gluteaControllers.controller('firstUseController', ['$scope', '$location', 'auth', '$timeout', firstUseController]);
	gluteaControllers.controller('overzichtController', ['$scope', '$location', 'mdEvent', 'auth', overzichtController]);
	gluteaControllers.controller('oefeningController', ['$scope', 'mdEvent', 'auth', '$stateParams', oefeningController]);
	gluteaControllers.controller('uitvoeringController', ['$scope', 'mdEvent', 'auth', '$stateParams', uitvoeringController]);

	function homeController($scope, $location, auth) {
		$scope.patient = auth.getCurrentPatient();
	}

	function firstUseController($scope, $location, auth, $timeout) {
		$scope.patient = {};
		$scope.confirmCredentials = function() {
			console.log($scope.patient);
			var id = $scope.patient.id;
			if (id !== undefined) {
				$scope.warning = null;
				$scope.checking = true;
				var promise = auth.checkPatientId($scope.patient.id);

				// If sign in fails
				promise.fail(function(message) {
					$scope.checking = false;
					$timeout(function() {
						$scope.warning = "Het ID komt niet overeen met een patient";
					}, 0);
				});
			} else {
				$timeout(function() {
					$scope.warning = "U heeft nog geen ID ingevuld";
				}, 0);
			}
		};

		$scope.logOut = function() {
			auth.signOut();
		}
	};

	function overzichtController($scope, $location, Event, auth) {
		$scope.patient = auth.getCurrentPatient();
		$scope.events = Event.$find();
	};

	function oefeningController($scope, Event, auth, $stateParams) {
		$scope.patient = auth.getCurrentPatient();
		$scope.oefening = Event.$find($stateParams.oefening);
	}

	function uitvoeringController($scope, Event, auth, $stateParams) {
		$scope.playing = false;
		$scope.repeats = 4;
		$scope.completed = false;

		//https://static.virtuagym.com/videos/xl001.mp4

		window.plugins.html5Video.initialize({
			"video1": "oefening.mp4"
		});

		$scope.play = function() {
			window.plugins.html5Video.play("video1", function() {
					$scope.decreaseRepeats();
			});
			$scope.playing = true;
			$scope.completed = true;
		};

		$scope.decreaseRepeats = function() {
				$scope.repeats--;
		};
	}


})();
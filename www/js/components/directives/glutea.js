(function() {
	'use strict';

	var gluteaDirectives = angular.module('gluteaDirectives', []);

	gluteaDirectives.directive('doctorsOffice', function() {
		return {
			restrict: 'E',
			transclude: true,
			templateUrl: 'templates/doctorsoffice.html'
		}
	});

	gluteaDirectives.filter('capitalize', function() {
		return function(input, all) {
			return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt) {
				return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
			}) : '';
		}
	});
})();
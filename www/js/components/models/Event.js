(function() {
	'use strict';

	function Event(eventData) {


		// SLOPPY --> REDESIGN SERVER SIDE
		if (!eventData.inspect) {
			_.extend(this, eventData);

			if (this.start) {
				var temp = this.start;
				var newstart = new Date(temp);
				this.start = newstart;
			}
			if(this.assignedExerciseId) {
				this.id = this.assignedExerciseId;
			}

			return;
		}

		// SINGLE EVENT --> PERFECT
		this.$unwrap(eventData);
	}

	Event.$factory = ['$timeout', '$location', 'mdResource', 'sStorage',
		function($timeout, $location, Resource, Storage) {
			_.extend(Event, {
				$storage: Storage,
				$timeout: $timeout,
				$location: $location,
				$$resourceSingle: new Resource('events'),
				$$resourceAgenda: new Resource('agenda')
			});
			return Event;
		}
	];

	angular.module('gluteaModels').factory('mdEvent', Event.$factory);

	Event.$find = function(id) {
		if (id) {
			var temp = Event.$storage.getObject(id);
			if (temp !== null) {
				return new Event(temp);
			} else {
				var eventData = this.$$resourceSingle.find(id);
				return new Event(eventData);
			}
		}

		var agendaData = this.$$resourceAgenda.find();
		return Event.$unwrapCollection(agendaData);
	};

	Event.prototype.$unwrap = function(eventData) {
		var self = this;

		this.$eventData = eventData;
		this.$eventData.then(function(data) {
			data.event.start = new Date(data.event.start);
			Event.$timeout(function() {
				_.extend(self, data);
				Event.$storage.setObject(self.id, data);
			});
		});
	};

	Event.$unwrapCollection = function(agendaData) {
		var collection = {};
		collection.$agendaData = agendaData;
		collection.events = [];

		agendaData.then(function(data) {
			var events = data.events;

			Event.$timeout(function() {
				_.reduce(events, function(c, event) {
					c.events.push(new Event(event));
					return c;
				}, collection);
			});
		});

		return collection.events;
	};

})();
(function() {
	'use strict';
	
	function Resource($http, api, path) {
		_.extend(this, {
			_path: path,
			_api: api,
			_http: $http
		});
	}

	Resource.$factory = ['$http', 'api',
		function($http, api) {
			return function(path) {
				return new Resource($http, api, path);
			};
		}
	];

	// Angular Registration
	angular.module('gluteaModels').factory('mdResource', Resource.$factory);

	Resource.prototype.find = function(uid) {
		var deffered = Q.defer();

		this._http.get(this.path(uid))
			.success(deffered.resolve)
			.error(deffered.reject);

		return deffered.promise;
	};

	Resource.prototype.path = function(uid) {
		var base = this._api.getResources() + this._path;
		return uid ? base + '/' + uid : base;
	};

	Resource.prototype.get = function(uid, action) {
		var deffered = Q.defer();

		var path = this.path(uid) + '/' + action;
		console.log(path);
		this._http.get(path)
			.success(deffered.resolve)
			.error(deffered.reject);

		return deffered.promise;
	};

	Resource.prototype.set = function(uid, action, data) {
		var deffered = Q.defer();

		var path = this.path(uid) + '/' + action;
		console.log(path);
		this._http.put(path, data)
			.success(deffered.resolve)
			.error(deffered.reject);

		return deffered.promise;
	};

	Resource.prototype.create = function(data) {
		var deffered = Q.defer();

		this._http.post(this.path(), data)
			.success(deffered.resolve)
			.error(function(data, status, headers, config) {
				deffered.reject(data);
			});

		return deffered.promise;
	};

	Resource.prototype.addById = function(data, pathId) {
		var deffered = Q.defer();
		var addPath = this.path() + '/' + pathId;

		this._http.post(addPath, data)
			.success(deffered.resolve)
			.error(function(data, status, headers, config) {
				deffered.reject(data);
			});

			console.log("POSTED!!!")

		return deffered.promise;
	};
})();
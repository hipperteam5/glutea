(function() {
	'use strict';

	var factories = angular.module('gluetaFactories', []);

	factories.factory('api', [api]);
	factories.factory('auth', ['$http', 'api', 'sStorage', '$timeout', '$location', auth]);
	factories.factory('sStorage', ['$window', sStorage]);

	function api() {
		/* DEVELOPMENT */
		// var server = "http://localhost:8080/";
		/* PRODUCTION */
			var server = "http://015.006.092.145.hva.nl:8080/";

		var resources = "secured/mobile/";

		return {
			getServer: function() {
				return server;
			},

			getResources: function() {
				return server + resources;
			}
		};
	}

	function auth($http, api, sStorage, $timeout, $location) {
		console.log("Loading auth");
		var currentPatient = null;
		var token = null;

		if (sStorage.getObject('patient') !== null) {
			currentPatient = sStorage.getObject("patient");
			token = sStorage.get("token");
		} else {
			console.log('No patient found, first use');
			$location.path('/firstuse');
		}

		return {
			getCurrentPatient: function() {
				if (currentPatient === null) {
					if (sStorage.getObject('patient') !== null) {
						currentPatient = sStorage.getObject("patient");
						token = sStorage.get("token");
						return currentPatient;
					} else {
						$timeout(function() {
							$location.path('/firstuse');
						});
					}
				} else {
					return currentPatient;
				}
			},

			checkPatientId: function(id) {
				var defer = Q.defer();
				$http.post(api.getServer() + "authenticate/" + id)
					.success(function(data) {
						//Handle Success
						console.log("SUCCESS!");
						console.log(data.hipperAccessToken);
						sStorage.clear();
						sStorage.setObject("patient", data.user); // Remember Patient
						sStorage.set("token", data.hipperAccessToken); // Remember Token
						$timeout(function() {
							$location.path('/home');
						}, 0);
					})
					.error(function(data) {
						//Handle Error
						defer.reject(data.message);
					});
				return defer.promise;
			},

			isAuthenticated: function() {
				return currentPatient !== null ? true : false;
			},

			signOut: function() {
				currentPatient = null;
				token = null;
				sStorage.clear();
				console.log('Cleared');
				$timeout(function() {
					$location.path('/firstuse');
				});
				alert('U bent uitgelogd.');
			}
		};
	}

	function sStorage($window) {
		return {
			set: function(key, value) {
				$window.localStorage[key] = value;
			},
			get: function(key, defaultValue) {
				return $window.localStorage[key] || defaultValue;
			},
			setObject: function(key, value) {
				$window.localStorage[key] = JSON.stringify(value);
			},
			getObject: function(key) {
				return JSON.parse($window.localStorage[key] || null);
			},
			clear: function() {
				$window.localStorage.clear();
			}
		};
	};



})();
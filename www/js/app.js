(function() {
  // Ionic Starter App

  // angular.module is a global place for creating, registering and retrieving Angular modules
  // 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
  // the 2nd parameter is an array of 'requires'
  // 'starter.services' is found in services.js
  // 'starter.controllers' is found in controllers.js
  var glutea = angular.module('glutea', [
    'ionic', 
    'angularMoment', 
    'ngCordova', 
    'gluteaModels', 
    'gluetaFactories', 
    'gluteaControllers', 
    'gluteaDirectives'
    ])

  .run(function($ionicPlatform, $cordovaSplashscreen, auth, amMoment) {
      $ionicPlatform.ready(function() {
        amMoment.changeLocale('nl');
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
          // org.apache.cordova.statusbar required
          StatusBar.styleDefault();
        }
        setTimeout(function() {
          $cordovaSplashscreen.hide()
        }, 0)

      });
    })
    .config(function($stateProvider, $urlRouterProvider) {

      // Ionic uses AngularUI Router which uses the concept of states
      // Learn more here: https://github.com/angular-ui/ui-router
      // Set up the various states which the app can be in.
      // Each state's controller can be found in controllers.js
      $stateProvider

      // setup an abstract state for the tabs directive
        .state('home', {
          url: "/home",
          abstract: false,
          views: {
            home: {
              templateUrl: "templates/home.html",
              controller: 'homeController'
            }
          }
        })
        .state('oefeningen', {
          abstract: true,
          url: '/oefeningen',
          views: {
            oefeningen: {
              template: '<ion-nav-view></ion-nav-view>'
            }
          }
        })
        .state('oefeningen.index', {
          url: '',
          templateUrl: 'templates/oefeningen/overzicht.html',
          controller: 'overzichtController'
        })
        .state('oefeningen.detail', {
          url: '/:oefening',
          templateUrl: 'templates/oefeningen/detail.html',
          controller: 'oefeningController'
        })
        .state('oefeningen.uitvoering', {
          url: '/uitvoering/:oefening',
          templateUrl: 'templates/oefeningen/uitvoering.html',
          controller: 'uitvoeringController'
        })
        // .state('oefeningen', {
        //   url: "/oefeningen",
        //   abstract: false,
        //   views: {
        //     oefeningen: {
        //       templateUrl: "templates/oefeningen/overzicht.html",
        //       controller: 'overzichtController'
        //     }
        //   }
        // })
        // .state('oefeningen.detail', {
        //   url: "/:oefening",
        //   abstract: false,
        //   templateUrl: "templates/oefeningen/detail.html",
        //   controller: 'oefeningController'

        // })
        .state('firstuse', {
          url: "/firstuse",
          abstract: false,
          views: {
            firstuse: {
              templateUrl: "templates/firstuse.html",
              controller: 'firstUseController'
            }
          }
        });

      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/home');

    })
    .factory('authInterceptor', function($rootScope, $q, $location) {
      return {
        request: function(config) {
          config.headers = config.headers || {};
          if (localStorage.getItem("token")) {
            var token = localStorage.getItem("token");
            config.headers.hipperaccesstoken = token;
          }
          return config;
        },
        response: function(response) {
          if (response.status === 401) {
            $location.path('/firstuse');
          }
          return response || $q.when(response);
        }
      };
    });

  glutea.config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });


})();

/*
  DEFINE GLOBAL MODULES
*/
(function() {

  'use strict';

  var gluteaModels = angular.module('gluteaModels', []);

})();